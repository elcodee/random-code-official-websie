import AboutSection from "@/CustomComponents/Homepages/AboutSection";
import ContactSection from "@/CustomComponents/Homepages/ContactSection";
import { FooterSection } from "@/CustomComponents/Homepages/FooterSection";
import HeroSection from "@/CustomComponents/Homepages/HeroSection";
import ProjectSection from "@/CustomComponents/Homepages/ProjectSection";
import ServicesSection from "@/CustomComponents/Homepages/ServicesSection";
import TestimonialSection from "@/CustomComponents/Homepages/TestimonialSection";
import NavigatioBarLayout from "@/Layouts/CustomLayout";
import { Link, Head } from "@inertiajs/react";
import { Container, Grid, Text, Title, createStyles, rem } from "@mantine/core";

const supTitle = "Services";
const description =
    "Its lungs contain an organ that creates electricity. The crackling sound of electricity can be heard when it exhales. Azurill’s tail is large and bouncy. It is packed full of the nutrients this Pokémon needs to grow.";
const data = [
    {
        image: "auditors",
        title: "Landing Page",
        description:
            "Azurill can be seen bouncing and playing on its big, rubbery tail",
    },
    {
        image: "lawyers",
        title: "Portofolio",
        description:
            "Fans obsess over the particular length and angle of its arms",
    },
    {
        image: "accountants",
        title: "Company Profile",
        description:
            "They divvy up their prey evenly among the members of their pack",
    },
    {
        image: "others",
        title: "E-Commerce",
        description: "Phanpy uses its long nose to shower itself",
    },
];

const image =
    "https://images.unsplash.com/photo-1508193638397-1c4234db14d8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=400&q=80";
const title = "Best forests to visit in North America";
const category = "nature";

//
const dataProject = [
    {
        image: "/assets/images/e-commerce.png",
        title: "Landing Page",
        category:
            "Azurill can be seen bouncing and playing on its big, rubbery tail",
    },
    {
        image: "https://images.unsplash.com/photo-1508193638397-1c4234db14d8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=400&q=80",
        title: "Portofolio",
        category:
            "Fans obsess over the particular length and angle of its arms",
    },
    {
        image: "https://images.unsplash.com/photo-1508193638397-1c4234db14d8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=400&q=80",
        title: "Company Profile",
        category:
            "They divvy up their prey evenly among the members of their pack",
    },
    {
        image: "https://images.unsplash.com/photo-1508193638397-1c4234db14d8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=400&q=80",
        title: "E-Commarce",
        category: "Phanpy uses its long nose to shower itself",
    },
];

const dataFooter = [
    {
        title: "About",
        links: [
            {
                label: "Features",
                link: "#",
            },
            {
                label: "Pricing",
                link: "#",
            },
            {
                label: "Support",
                link: "#",
            },
            {
                label: "Forums",
                link: "#",
            },
        ],
    },
    {
        title: "Project",
        links: [
            {
                label: "Contribute",
                link: "#",
            },
            {
                label: "Media assets",
                link: "#",
            },
            {
                label: "Changelog",
                link: "#",
            },
            {
                label: "Releases",
                link: "#",
            },
        ],
    },
    {
        title: "Community",
        links: [
            {
                label: "Join Discord",
                link: "#",
            },
            {
                label: "Follow on Twitter",
                link: "#",
            },
            {
                label: "Email newsletter",
                link: "#",
            },
            {
                label: "GitHub discussions",
                link: "#",
            },
        ],
    },
];

const useStyles = createStyles((theme) => ({
    supTitle: {
        textAlign: "center",
        textTransform: "uppercase",
        fontWeight: 800,
        fontSize: theme.fontSizes.sm,
        color: theme.fn.variant({ variant: "light", color: theme.primaryColor })
            .color,
        letterSpacing: rem(0.5),
    },

    title: {
        lineHeight: 1,
        textAlign: "center",
        marginTop: theme.spacing.xl,
    },

    highlight: {
        backgroundColor: theme.fn.variant({
            variant: "light",
            color: theme.primaryColor,
        }).background,
        padding: rem(5),
        paddingTop: 0,
        // marginBottom: rem(30),
        borderRadius: theme.radius.sm,
        display: "inline-block",
        color: theme.colorScheme === "dark" ? theme.white : "inherit",
    },
}));

export default function Welcome({ route_path, acl, auth }) {
    const { classes } = useStyles();

    return (
        <>
            <Head title="CV Random Code" />
            <NavigatioBarLayout routeAccess={acl} routeName={route_path} />

            <Container size="80rem" px={20}>
                <HeroSection />
                <AboutSection />
                <ServicesSection
                    supTitle={supTitle}
                    description={description}
                    data={data}
                />

                <Text
                    className={`${classes.supTitle} animate__animated animate__fadeInDown animate__delay-0s`}
                    mt={50}
                    id="project-section"
                >
                    PROJECTS
                </Text>

                <Title
                    className={`${classes.title} animate__animated animate__fadeInDown animate__delay-1s`}
                    mb={50}
                    order={2}
                >
                    {/* PharmLand is <span className={classes.highlight}>not</span> just for
                    pharmacists */}
                    Berikut Adalah{" "}
                    <span className={classes.highlight}>Project</span> Yang
                    Sudah Kami Kerjakan
                </Title>

                <Grid className="animate__animated animate__fadeInDown animate__delay-2s">
                    {dataProject &&
                        dataProject.map((item, index) => {
                            return (
                                <Grid.Col md={6} lg={3}>
                                    <ProjectSection
                                        image={item.image}
                                        title={item.title}
                                        category={item.category}
                                    />
                                </Grid.Col>
                            );
                        })}
                </Grid>

                <Text
                    className={`${classes.supTitle} animate__animated animate__fadeInDown animate__delay-0s`}
                    mt={70}
                    id="testimonials-section"
                >
                    TESTIMONIALS
                </Text>

                <Title
                    className={`${classes.title} animate__animated animate__fadeInDown animate__delay-1s`}
                    mb={50}
                    order={2}
                >
                    {/* PharmLand is <span className={classes.highlight}>not</span> just for
                    pharmacists */}
                    Berikut Adalah{" "}
                    <span className={classes.highlight}>Feedback</span> Dari
                    Client Kami
                </Title>

                <Container
                    size="50rem"
                    px={1}
                    mb={70}
                    className="animate__animated animate__fadeInDown animate__delay-2s"
                >
                    <TestimonialSection />
                </Container>

                <Text
                    className={`${classes.supTitle} animate__animated animate__fadeInDown animate__delay-0s`}
                    mt={70}
                    id="contact-section"
                >
                    CONTACT US
                </Text>

                <Title
                    className={`${classes.title} animate__animated animate__fadeInDown animate__delay-1s`}
                    mb={50}
                    order={2}
                >
                    Ingin Membuat{" "}
                    <span className={classes.highlight}>
                        Bussiness Logic / Media Digital
                    </span>{" "}
                    Konsultasikan Sekarang
                </Title>

                <ContactSection />
                {/* ohh jadi gitu */}
            </Container>
            <FooterSection data={dataFooter} />
        </>
    );
}
