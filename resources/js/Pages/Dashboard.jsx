import StatsAdmin from '@/CustomComponents/AdminPages/StatsAdmin';
import CustomAdminLayout from '@/Layouts/CustomAdminLayout';
import CustomAdminMobileLayout from '@/Layouts/CustomAdminMobileLayout';
import { Head } from '@inertiajs/react';

export default function Dashboard({ auth }) {
    return (
        <>
            <CustomAdminMobileLayout />

            <CustomAdminLayout>
                <Head title="Dashboard" />

                <StatsAdmin />
                {/* <div className="py-12">
                    <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                            <div className="p-6 text-gray-900">You're logged in!</div>
                        </div>
                    </div>
                </div> */}
            </CustomAdminLayout>
        </>
    );
}
