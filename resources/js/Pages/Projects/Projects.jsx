import { FooterSection } from '@/CustomComponents/Homepages/FooterSection';
import CarouselProject from '@/CustomComponents/ProjectPages/CarouselProject';
import ProjectCard from '@/CustomComponents/ProjectPages/ProjectCard';
import ProjectEmpty from '@/CustomComponents/ProjectPages/ProjectEmpty';
import NavigatioBarLayout from '@/Layouts/CustomLayout';
import { Head } from '@inertiajs/react';
import { Container, Grid, Text, Title, createStyles, rem } from '@mantine/core';

const dataFooter = [
    {
        title: "About",
        links: [
            {
                label: "Features",
                link: "#"
            },
            {
                label: "Pricing",
                link: "#"
            },
            {
                label: "Support",
                link: "#"
            },
            {
                label: "Forums",
                link: "#"
            }
        ]
    },
    {
        title: "Project",
        links: [
            {
                label: "Contribute",
                link: "#"
            },
            {
                label: "Media assets",
                link: "#"
            },
            {
                label: "Changelog",
                link: "#"
            },
            {
                label: "Releases",
                link: "#"
            }
        ]
    },
    {
        title: "Community",
        links: [
            {
                label: "Join Discord",
                link: "#"
            },
            {
                label: "Follow on Twitter",
                link: "#"
            },
            {
                label: "Email newsletter",
                link: "#"
            },
            {
                label: "GitHub discussions",
                link: "#"
            }
        ]
    }
]

const useStyles = createStyles(theme => ({
    supTitle: {
        textAlign: "center",
        textTransform: "uppercase",
        fontWeight: 800,
        fontSize: theme.fontSizes.sm,
        color: theme.fn.variant({ variant: "light", color: theme.primaryColor })
            .color,
        letterSpacing: rem(0.5)
    },

    title: {
        lineHeight: 1,
        textAlign: "center",
        marginTop: theme.spacing.xl
    },

    highlight: {
        backgroundColor: theme.fn.variant({
            variant: "light",
            color: theme.primaryColor
        }).background,
        padding: rem(5),
        paddingTop: 0,
        // marginBottom: rem(30),
        borderRadius: theme.radius.sm,
        display: "inline-block",
        color: theme.colorScheme === "dark" ? theme.white : "inherit"
    }
}));



export default function Project({ route_path, acl, title, datas }) {
    const { classes } = useStyles();

    return (
        <>
            <Head title="CV Random Code" />
            <NavigatioBarLayout routeAccess={acl} routeName={route_path} />

            <Container size="80rem" mt={80} px={20}>
                {
                    datas?.length !== 0 ?
                        <>
                            <Title className={`${classes.title} animate__animated animate__fadeInDown animate__delay-1s`} order={2}>
                                <span className={classes.highlight}>{title}</span>
                            </Title>
                        </> : null
                }

                <Grid mt={30} className='animate__animated animate__fadeInDown'>
                    {
                        datas?.length !== 0 ? datas.map((item, index) => {
                            return (
                                <Grid.Col md={6} lg={3}>
                                    <ProjectCard data={item} />
                                </Grid.Col>
                            )
                        }) : <ProjectEmpty title={title} />
                    }
                </Grid>

            </Container>
            <FooterSection data={dataFooter} />
        </>
    );
}
