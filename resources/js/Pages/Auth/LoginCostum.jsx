import InputError from "@/Components/InputError";
import { useForm } from "@inertiajs/react";
import { HiEye, HiEyeOff } from "react-icons/hi";
import {
    TextInput,
    PasswordInput,
    Checkbox,
    Anchor,
    Paper,
    Title,
    Text,
    Container,
    Group,
    Button,
    Image,
    Switch,
    createStyles,
    rem,
} from "@mantine/core";
import { useEffect, useState } from "react";
import { useDisclosure } from "@mantine/hooks";

const useStyles = createStyles((theme) => ({
    wrapper: {
        minHeight: rem(927),
        zIndex: -1,
        backgroundSize: "cover",
        backgroundImage:
            "url(https://a-static.besthdwallpaper.com/beach-sunset-sky-clouds-scenery-wallpaper-2800x1050-80754_88.jpg)",
    },
}));
export default function LoginCostum({ status, canResetPassword }) {
    const { classes } = useStyles();
    const [visible, { toggle }] = useDisclosure(false);
    const { data, setData, post, processing, errors, reset } = useForm({
        email: "",
        password: "",
        remember: false,
    });

    useEffect(() => {
        return () => {
            reset("password");
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route("login"));
    };

    return (
        <div className={classes.wrapper}>
            <Container size={420} py={200}>
                <Title
                    align="center"
                    sx={(theme) => ({
                        fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                        fontWeight: 900,
                        color: `white`,
                    })}
                >
                    Welcome back bro!
                </Title>

                <Paper withBorder shadow="md" p={30} mt={30} radius="md">
                    {status && (
                        <div className="mb-4 font-medium text-sm text-green-600">
                            {status}
                        </div>
                    )}

                    <TextInput
                        label="Email"
                        placeholder="you@random.dev"
                        id="email"
                        type="email"
                        name="email"
                        value={data.email}
                        isFocused={true}
                        onChange={(e) => setData("email", e.target.value)}
                        required
                    />
                    <InputError message={errors.email} className="mt-2" />

                    <PasswordInput
                        label="Password"
                        placeholder="Your password"
                        id="password"
                        name="password"
                        value={data.password}
                        autoComplete="current-password"
                        onChange={(e) => setData("password", e.target.value)}
                        required
                        visible={visible}
                        onVisibilityChange={toggle}
                        mt="md"
                    />
                    <InputError message={errors.password} className="mt-2" />

                    <Group position="apart" mt="lg">
                        <Switch
                            label="Ingat saya"
                            name="remember"
                            checked={data.remember}
                            onChange={(e) =>
                                setData("remember", e.target.checked)
                            }
                        />
                    </Group>
                    <Button
                        variant="default"
                        fullWidth
                        mt="xl"
                        onClick={submit}
                    >
                        Sign in
                    </Button>
                </Paper>
            </Container>
        </div>
    );
}
