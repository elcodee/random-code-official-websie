import { useDisclosure } from "@mantine/hooks"
import { Drawer, Button, Group } from "@mantine/core"

export default function MobileMenuDrawer({ opened, open, close }) {
    return (
        <>
            <Drawer
                opened={opened}
                onClose={close}
                title="Authentication"
                transitionProps={{
                    transition: "rotate-left",
                    duration: 150,
                    timingFunction: "linear"
                }}
            >
                {/* Drawer content */}
            </Drawer>
        </>
    )
}
