import { Link } from "@inertiajs/react";
import {
    createStyles,
    Image,
    Container,
    Title,
    Text,
    Button,
    SimpleGrid,
    rem
} from "@mantine/core"
//   import image from "./image.svg"

const useStyles = createStyles(theme => ({
    root: {
        paddingTop: rem(10),
        paddingBottom: rem(80)
    },

    title: {
        fontWeight: 900,
        fontSize: rem(34),
        marginBottom: theme.spacing.md,
        fontFamily: `Greycliff CF, ${theme.fontFamily}`,

        [theme.fn.smallerThan("sm")]: {
            fontSize: rem(32)
        }
    },

    control: {
        [theme.fn.smallerThan("sm")]: {
            width: "100%"
        }
    },

    mobileImage: {
        [theme.fn.largerThan("sm")]: {
            display: "none"
        }
    },

    desktopImage: {
        [theme.fn.smallerThan("sm")]: {
            display: "none"
        }
    }
}))

export default function ProjectEmpty({ title }) {
    const { classes } = useStyles()

    return (
        <Container className={classes.root}>
            <SimpleGrid
                spacing={80}
                cols={2}
                breakpoints={[{ maxWidth: "sm", cols: 1, spacing: 10 }]}
            >
                <Image src={"https://img.freepik.com/free-vector/young-working-woman-use-magnifying-glass-searching-information-folder_1150-51045.jpg"} className={classes.mobileImage} />
                <div>
                    <Title className={classes.title}>{title} Sedang Tidak Tersedia {`:(`}</Title>
                    <Text color="dimmed" size="lg">
                        Kami akan segera membuatkan untuk anda.
                    </Text>
                    <Link href="/#services-section">
                        <Button
                            variant="outline"
                            size="md"
                            mt="xl"
                            className={classes.control}
                        >
                            Lihat Service Lainnya
                        </Button>
                    </Link>
                </div>
                <Image src={"https://img.freepik.com/free-vector/young-working-woman-use-magnifying-glass-searching-information-folder_1150-51045.jpg"} className={classes.desktopImage} />
            </SimpleGrid>
        </Container>
    )
}
