import { IconBookmark, IconHeart, IconShare } from "@tabler/icons-react"
import {
    Card,
    Image,
    Text,
    ActionIcon,
    Badge,
    Group,
    Center,
    Avatar,
    createStyles,
    rem,
    Button
} from "@mantine/core"

const useStyles = createStyles(theme => ({
    card: {
        position: "relative",
        backgroundColor:
            theme.colorScheme === "dark" ? theme.colors.dark[7] : theme.white
    },

    rating: {
        position: "absolute",
        top: theme.spacing.xs,
        right: rem(12),
        pointerEvents: "none"
    },

    title: {
        display: "block",
        marginTop: theme.spacing.md,
        marginBottom: rem(5)
    },

    action: {
        backgroundColor:
            theme.colorScheme === "dark"
                ? theme.colors.dark[6]
                : theme.colors.gray[0],
        ...theme.fn.hover({
            backgroundColor:
                theme.colorScheme === "dark"
                    ? theme.colors.dark[5]
                    : theme.colors.gray[1]
        })
    },

    footer: {
        marginTop: theme.spacing.md
    }
}))

export default function ProjectCard({ data }) {
    const { classes, cx, theme } = useStyles()
    const linkProps = { href: data?.link, target: "_blank", rel: "noopener noreferrer" }

    return (
        <Card
            withBorder
            radius="md"
            className={cx(classes.card)}
        >
            <Card.Section>
                <a {...linkProps}>
                    <Image src={data?.image[0]} height={180} />
                </a>
            </Card.Section>

            <Badge
                className={classes.rating}
                variant="gradient"
                gradient={{ from: data?.content?.stok ? "red" : "green", to: data?.content?.stok ? "orange" : "cyan" }}
            >
                {data?.content?.stok ? "Availabe" : "Coming Soon"}
            </Badge>

            <Text className={classes.title} fw={500} component="a" {...linkProps}>
                {data?.content?.title}
            </Text>

            <Text fz="sm" color="dimmed" lineClamp={3}>
                {data?.content?.desc}
            </Text>

            <Group position="apart" className={classes.footer}>
                <Center>
                    <Text fz="sm" inline>
                        Rp {parseInt(data?.content?.price).toLocaleString('id')}
                    </Text>
                </Center>

                <Group spacing={8} mr={0}>
                    <Button variant="default" gradient={{ from: 'teal', to: 'blue', deg: 60 }}>Order Sekarang</Button>
                </Group>
            </Group>
        </Card>
    )
}
