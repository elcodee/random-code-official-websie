import { Link } from "@inertiajs/react";
import {
    createStyles,
    Image,
    Container,
    Title,
    Button,
    Group,
    Text,
    List,
    ThemeIcon,
    rem
} from "@mantine/core"
import { IconCheck } from "@tabler/icons-react"
import { IoArrowForward } from "react-icons/io5";

const useStyles = createStyles(theme => ({
    inner: {
        display: "flex",
        justifyContent: "space-between",
        paddingTop: `calc(${theme.spacing.xl} * 4)`,
        paddingBottom: `calc(${theme.spacing.xl} * 4)`
    },

    content: {
        maxWidth: rem(480),
        marginRight: `calc(${theme.spacing.xl} * 3)`,

        [theme.fn.smallerThan("md")]: {
            maxWidth: "100%",
            marginRight: 0
        }
    },

    title: {
        color: theme.colorScheme === "dark" ? theme.white : theme.black,
        fontFamily: `Greycliff CF, ${theme.fontFamily}`,
        fontSize: rem(44),
        lineHeight: 1.2,
        fontWeight: 900,

        [theme.fn.smallerThan("xs")]: {
            fontSize: rem(28)
        }
    },

    control: {
        [theme.fn.smallerThan("md")]: {
            flex: 1
        }
    },

    image: {
        flex: 1,
        width: 100,
        [theme.fn.smallerThan("lg")]: {
            display: "block"
        }
    },

    highlight: {
        position: "relative",
        backgroundColor: theme.fn.variant({
            variant: "light",
            color: theme.primaryColor
        }).background,
        borderRadius: theme.radius.sm,
        padding: `${rem(1)} ${rem(10)}`
    }
}))

export default function HeroSection() {
    const { classes } = useStyles()
    return (
        <div>
            <Container id="hero-section">
                <div className={classes.inner}>
                    <div className={classes.content}>
                        <Title className={`${classes.title} animate__animated animate__flipInX`}>
                            Jasa Pembuatan Website <span className={classes.highlight}>Murah Dan Terpercaya</span>
                        </Title>
                        <Text className="animate__animated animate__fadeInDown animate__delay-1s" color="dimmed" mt="md">
                            Kami siap membantu mengembangkan bisnis anda dengan membuat sebuah website sesuai kebutuhan anda
                        </Text>

                        <List
                            mt={30}
                            spacing="sm"
                            size="sm"
                            icon={
                                <ThemeIcon size={20} radius="xl">
                                    <IconCheck size={rem(12)} stroke={1.5} />
                                </ThemeIcon>
                            }
                        >
                            <List.Item className="animate__animated animate__fadeInDown animate__delay-2s">
                                <b>Terpercaya</b> – Berusaha menjadi Developer terpercaya bagi semua konsumen
                            </List.Item>
                            <List.Item className="animate__animated animate__fadeInDown animate__delay-3s">
                                <b>Harga Bersahabat</b> – manfaat yang diperoleh jauh lebih besar daripada biaya yang diinvestasikan
                            </List.Item>
                            <List.Item className="animate__animated animate__fadeInDown animate__delay-4s">
                                <b>Garansi Support Bug</b> – kami akan perbaiki Bug tersebut sampai terselesaikan tanpa harus menambah biaya
                            </List.Item>
                            <List.Item className="animate__animated animate__fadeInDown animate__delay-5s">
                                <b>Group Support</b> – Kami akan support penuh demi kelancaran project, Dengan membuat group chat
                            </List.Item>
                        </List>

                        <Group mt={30}>
                            <Link href="/#about-section">
                                <Button
                                    variant="default"
                                    radius="xl"
                                    size="md"
                                    className={`${classes.control} animate__animated animate__flipInX animate__delay-4s`}
                                >
                                    Tentang Kami &nbsp; <IoArrowForward />
                                </Button>
                            </Link>
                        </Group>
                    </div>
                    <Image src={"https://raw.githubusercontent.com/AlaeddineMessadi/AlaeddineMessadi/main/web-developer-chilling.gif"} className={`${classes.image} animate__animated animate__fadeIn animate__delay-1s`} />
                </div>
            </Container>
        </div>
    )
}
