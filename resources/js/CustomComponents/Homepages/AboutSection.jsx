import {
    createStyles,
    Container,
    Text,
    Button,
    Group,
    rem
} from "@mantine/core"
import { GithubIcon } from "@mantine/ds"

const useStyles = createStyles(theme => ({
    wrapper: {
        position: "relative",
        boxSizing: "border-box",
        backgroundColor:
            theme.colorScheme === "dark" ? theme.colors.dark[8] : theme.white
    },

    inner: {
        position: "relative",
        paddingTop: rem(1),
        paddingBottom: rem(120),

        [theme.fn.smallerThan("sm")]: {
            paddingBottom: rem(80),
            paddingTop: rem(50)
        }
    },

    title: {
        fontFamily: `Greycliff CF, ${theme.fontFamily}`,
        fontSize: rem(40),
        fontWeight: 900,
        lineHeight: 1.1,
        margin: 0,
        padding: 0,
        color: theme.colorScheme === "dark" ? theme.white : theme.black,

        [theme.fn.smallerThan("sm")]: {
            fontSize: rem(35),
            lineHeight: 1.2
        }
    },

    description: {
        marginTop: theme.spacing.xl,
        fontSize: rem(20),

        [theme.fn.smallerThan("sm")]: {
            fontSize: rem(18)
        }
    },

    controls: {
        marginTop: `calc(${theme.spacing.xl} * 2)`,

        [theme.fn.smallerThan("sm")]: {
            marginTop: theme.spacing.xl
        }
    },

    control: {
        height: rem(54),
        paddingLeft: rem(38),
        paddingRight: rem(38),

        [theme.fn.smallerThan("sm")]: {
            height: rem(54),
            paddingLeft: rem(18),
            paddingRight: rem(18),
            flex: 1
        }
    }
}))

export default function AboutSection() {
    const { classes } = useStyles()

    return (
        <div className={classes.wrapper} id="about-section">
            <Container size={1000} className={classes.inner}>
                <h1 className={`${classes.title} animate__animated animate__fadeInDown animate__delay-1s`}>
                    <Text
                        component="span"
                        variant="gradient"
                        gradient={{ from: "blue", to: "cyan" }}
                        inherit
                    >
                        TENTANG KAMI
                    </Text>
                    <br />
                    Perusahaan yang dipercaya sejak tahun 2020 dalam bidang teknologi
                </h1>

                <Text className={`${classes.description} animate__animated animate__fadeInDown animate__delay-2s`} color="dimmed">
                    CV Random Code adalah sebuah perusahaan yang bergerak dibidang teknologi dengan tim pengembang yang sudah berpengalaman sejak tahun 2020 dalam membuat website.
                </Text>

                <Text className={`${classes.description} animate__animated animate__fadeInDown animate__delay-3s`} color="dimmed">
                    Fokus utama kami adalah memberikan solusi teknologi terbaik bagi pelanggan melalui layanan yang berkualitas dan inovatif. Kami akan terus berkembang dan berinovasi untuk memberikan layanan terbaik bagi client kami.
                </Text>

                {/* <Group className={classes.controls}>
                    <Button
                        size="xl"
                        className={`${classes.control} animate__animated animate__flipInX animate__delay-2s`}
                        variant="default"
                        gradient={{ from: "blue", to: "cyan" }}
                    >
                        Get started
                    </Button>

                    <Button
                        component="a"
                        href="https://github.com/mantinedev/mantine"
                        size="xl"
                        variant="default"
                        className={`${classes.control} animate__animated animate__flipInX animate__delay-3s`}
                        leftIcon={<GithubIcon size={20} />}
                    >
                        GitHub
                    </Button>
                </Group> */}
            </Container>
        </div>
    )
}
