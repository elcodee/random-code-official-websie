import {
  Image,
  Text,
  Container,
  ThemeIcon,
  Title,
  SimpleGrid,
  createStyles,
  rem,
  Button
} from "@mantine/core"
import { useDisclosure } from "@mantine/hooks";
import { Link } from "@inertiajs/react";
//   import IMAGES from "./images"

const useStyles = createStyles(theme => ({
  wrapper: {
    paddingTop: rem(50),
    paddingBottom: rem(50)
  },

  item: {
    display: "flex"
  },

  itemIcon: {
    padding: theme.spacing.xs,
    marginRight: theme.spacing.md
  },

  itemTitle: {
    marginBottom: `calc(${theme.spacing.xs} / 2)`
  },

  supTitle: {
    textAlign: "center",
    textTransform: "uppercase",
    fontWeight: 800,
    fontSize: theme.fontSizes.sm,
    color: theme.fn.variant({ variant: "light", color: theme.primaryColor })
      .color,
    letterSpacing: rem(0.5)
  },

  title: {
    lineHeight: 1,
    textAlign: "center",
    marginTop: theme.spacing.xl
  },

  description: {
    textAlign: "center",
    marginTop: theme.spacing.xs
  },

  highlight: {
    backgroundColor: theme.fn.variant({
      variant: "light",
      color: theme.primaryColor
    }).background,
    padding: rem(5),
    paddingTop: 0,
    borderRadius: theme.radius.sm,
    display: "inline-block",
    color: theme.colorScheme === "dark" ? theme.white : "inherit"
  }
}))

export default function ServicesSection({ supTitle, description, data }) {
  const { classes } = useStyles();
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <Container size={700} className={classes.wrapper} id="services-section">
      <Text className={`${classes.supTitle} animate__animated animate__fadeInDown animate__delay-0s`}>{supTitle}</Text>

      <Title className={`${classes.title} animate__animated animate__fadeInDown animate__delay-1s`} mb={50} order={2}>
        Berikut Beberapa <span className={classes.highlight}>Services</span> Yang Kami Sediakan
      </Title>

      <SimpleGrid
        cols={3}
        spacing={30}
        breakpoints={[{ maxWidth: 1000, cols: 1, spacing: 30 }]}
        style={{ marginTop: 20 }}
      >
        {
          data?.map((item, index) => {

            return (
              <div className={classes.item} key={index + 1}>
                <ThemeIcon
                  variant="light"
                  className={`${classes.itemIcon} animate__animated animate__bounceIn animate__delay-4s`}
                  size={60}
                  radius="md"
                >
                  <Image src={""} />
                </ThemeIcon>

                <div className="animate__animated animate__fadeInDown animate__delay-3s">
                  <Text fw={700} fz="lg" className={classes.itemTitle}>
                    {item.title}
                  </Text>
                  <Text c="dimmed">{item.description}</Text>
                  <Link href={route('project', item.title)}>
                    <Button
                      size="sm"
                      className={`mt-5 animate__animated animate__flipInX animate__delay-2s`}
                      variant="default"
                      gradient={{ from: "blue", to: "cyan" }}
                    >
                      Lihat Detail
                    </Button>
                  </Link>
                  {/* <ModalProjectsSection data={item} opened={opened} open={open} close={close} btn={(<Button
                    size="sm"
                    className={`mt-5 animate__animated animate__flipInX animate__delay-2s`}
                    variant="default"
                    gradient={{ from: "blue", to: "cyan" }}
                    onClick={open}
                    key={index + 1}
                  >
                    Lihat Detail
                  </Button>)} /> */}
                </div>
              </div>
            )
          })
        }
      </SimpleGrid>
    </Container>
  )
}
