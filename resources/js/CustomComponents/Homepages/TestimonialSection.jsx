import {
    createStyles,
    Image,
    Card,
    Text,
    Group,
    Button,
    getStylesRef,
    rem
  } from "@mantine/core"
  import { Carousel } from "@mantine/carousel"
  import { IconStar } from "@tabler/icons-react"
  
  const useStyles = createStyles(theme => ({
    price: {
      color: theme.colorScheme === "dark" ? theme.white : theme.black
    },
  
    carousel: {
      "&:hover": {
        [`& .${getStylesRef("carouselControls")}`]: {
          opacity: 1
        }
      }
    },
  
    carouselControls: {
      ref: getStylesRef("carouselControls"),
      transition: "opacity 150ms ease",
      opacity: 0
    },
  
    carouselIndicator: {
      width: rem(4),
      height: rem(4),
      transition: "width 250ms ease",
  
      "&[data-active]": {
        width: rem(16)
      }
    }
  }))
  
  const images = [
    "https://images.unsplash.com/photo-1598928506311-c55ded91a20c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=720&q=80",
    "https://images.unsplash.com/photo-1567767292278-a4f21aa2d36e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=720&q=80",
    "https://images.unsplash.com/photo-1605774337664-7a846e9cdf17?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=720&q=80",
    "https://images.unsplash.com/photo-1554995207-c18c203602cb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=720&q=80",
    "https://images.unsplash.com/photo-1616486029423-aaa4789e8c9a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=720&q=80"
  ]
  
  export default function TestimonialSection() {
    const { classes } = useStyles()
  
    const slides = images.map(image => (
      <Carousel.Slide key={image}>
        <Image src={image} height={220} />
      </Carousel.Slide>
    ))
  
    return (
      <Card radius="md" withBorder padding="xl">
        <Card.Section>
          <Carousel
            withIndicators
            loop
            speed={10}
            classNames={{
              root: classes.carousel,
              controls: classes.carouselControls,
              indicator: classes.carouselIndicator
            }}
          >
            {slides}
          </Carousel>
        </Card.Section>
  
        <Group position="apart" mt="lg" className="animate__animated animate__flipInX animate__delay-3s">
          <Text fw={500} fz="lg">
            Nayta 
          </Text>
  
          <Group spacing={5}>
            <IconStar size="1rem" />
            <Text fz="xs" fw={500}>
              4.78
            </Text>
          </Group>
        </Group>
  
        <Text fz="sm" c="dimmed" mt="sm" className="animate__animated animate__flipInX animate__delay-3s">
        Saya sudah order berulang kali di layanan web hosting ini, tidak pernah ada kecewa, apalagi support yang sangat cepat dan ramah.
        </Text>
  
        <Group position="apart" mt="md" className="animate__animated animate__flipInX animate__delay-3s">
          <div>
            <Text fz="xl" span fw={500} className={classes.price}>
              397$
            </Text>
            <Text span fz="sm" c="dimmed">
              {" "}
              / night
            </Text>
          </div>
  
          <Button variant="default" radius="md" className="animate__animated animate__lightSpeedInRight animate__delay-3s">Join Now</Button>
        </Group>
      </Card>
    )
  }
  