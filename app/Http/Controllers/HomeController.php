<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use Illuminate\Support\Facades\Route;
use App\Models\AccessControlList;
use App\Models\Projects;
use App\Models\ProjectCarousel;

class HomeController extends Controller
{
    /**
     * Display Homepage.
     */
    public function index()
    {
        $route_name = Route::currentRouteName();
        $acl = AccessControlList::all();

        return Inertia::render('Welcome', [
            'route_path' => $route_name,
            'acl' => $acl
        ]);
    }

    /**
     * Display Projects Page.
     */
    public function indexProject($name)
    {
        $route_name = Route::currentRouteName();
        $acl = AccessControlList::all();
        $projects = Projects::where('category', $name)->get();
        $datas = array();
        
        foreach ($projects as $key => $value) {
            $projectsImage = ProjectCarousel::where('id', $value->id)->first();
            $datas[] = [
                'content' => $value,
                'image' => [
                    0 => $projectsImage['image_1'],
                    1 => $projectsImage['image_2'],
                    2 => $projectsImage['image_3'],
                    3 => $projectsImage['image_4'],
                    4 => $projectsImage['image_5'],
                ]
            ];
        }

        return Inertia::render('Projects/Projects', [
            'route_path' => $route_name,
            'acl' => $acl,
            'title' => $name,
            'datas' => $datas

        ]);
    }
}
